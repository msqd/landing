/*global $, Handlebars, Handler, C, moment, Utility, Worker, Push*/
var Compiler = (function () {
    "use strict";

    var Handles = (function () {

        function compileTemplate(context, template, target) {
            return $(Handlebars.compile(template)(context)).appendTo(target);
        }

        function compileFailure(location, reason) {
            Utility.Failure(reason + " for  '" + Utility.Titles(location) + "'.");
        }

        function compileSuccess(location, reason) {
            Utility.Success(reason + " for '" + Utility.Titles(location) + "'.");
        }

        function compileNeutral(location, reason) {
            Utility.Neutral(reason + " for '" + Utility.Titles(location) + "'.");
        }

        function compileLowercase(value) {
            return String(value).toLowerCase();
        }

        function compileForename(value) {
            if (value.includes(" ")) {
                return value.substring(0, value.indexOf(" "));
            }

            return value;
        }

        function compileConditional(v1, operator, v2, options) {
            switch (operator) {
            case '===':
                return (v1 === v2) ? options.fn(v1) : options.inverse(v1);
            case '!==':
                return (v1 !== v2) ? options.fn(v1) : options.inverse(v1);
            case '<':
                return (v1 < v2) ? options.fn(v1) : options.inverse(v1);
            case '<=':
                return (v1 <= v2) ? options.fn(v1) : options.inverse(v1);
            case '>':
                return (v1 > v2) ? options.fn(v1) : options.inverse(v1);
            case '>=':
                return (v1 >= v2) ? options.fn(v1) : options.inverse(v1);
            case '&&':
                return (v1 && v2) ? options.fn(v1) : options.inverse(v1);
            case '||':
                return (v1 || v2) ? options.fn(v1) : options.inverse(v1);
            default:
                return options.inverse(v1);
            }
        }

        function compileNotificiation(title, description, callback) {
            Push.create(title, {
                body: description,
                icon: 'cdn/images/icon.png',
                timeout: 4000,
                onClick: callback
            });
        }
        
        function compileShortner(words, breakpoint) {
            words = words.split(' ');

            var result = "",
                first = "",
                last = "";

            words.forEach(function (word) {
                
                first = word.substring(0, 1) + "<span class='" + breakpoint + "'>";
                last = word.substring(1, word.length) + "</span>";

                result += " " + first + last;

            });

            return result.trim();
        }

        return {
            Template: compileTemplate,
            Failure: compileFailure,
            Neutral: compileNeutral,
            Success: compileSuccess,
            Lowercase: compileLowercase,
            Forename: compileForename,
            Conditional: compileConditional,
            Notifications: compileNotificiation,
            Shortener: compileShortner
        };

    }());

    Handlebars.registerHelper('Thousands', Utility.Thousands);
    Handlebars.registerHelper('toLowerCase', Handles.Lowercase);
    Handlebars.registerHelper('Forename', Handles.Forename);
    Handlebars.registerHelper('toTitleCase', Utility.Titles);
    Handlebars.registerHelper('Conditional', Handles.Conditional);
    Handlebars.registerHelper('Shortener', Handles.Shortener);

    function compileBase() {
        
        var baseTemplate = Utility.Storage('baseTemplate'),
            baseSchema = Utility.Storage('baseSchema');
        
        function compileBaseCompile(template, context) {
            
            // Get the Compilation
            var baseCompilation = Handles.Template(context, template, $('#user-area').html(null)),
                baseCounter = 0;

            // Cast the Objects
            baseCompilation.find('#notifications-clear > a').click(Utility.Notifications.Caret);
            baseCompilation.find('#exit-button').click(Compiler.Exit);
            baseCompilation.find('#notifications-refresh-button').click(Compiler.Refresh);
            baseCompilation.find('#notifications-clear-button').click(Utility.Notifications.Clear);
            baseCompilation.find('.btn-link').click(Utility.Navigation);
            baseCompilation.find('[data-tooltip="tooltip"]').tooltip({
                trigger: "hover"
            });
            baseCompilation.find('[data-tooltip="tooltip"]').click(function () {
                setTimeout(function () {
                    baseCompilation.find('[data-tooltip="tooltip"]').tooltip("hide");
                }, 100);
            });
            
            if (context.user && !Utility.Storage('baseNotifications')) {
                
                context.user.notifications.forEach(function (notification) {
                    if (notification.important) {
                        
                        // Log Success
                        Handles.Success(notification.title, "Generated an important notification");
                        
                        // Create Notification
                        Handles.Notifications(notification.title, notification.description);
                        
                    } else {
                        baseCounter += 1;
                    }
                });
                
                // Create Notification
                Handles.Notifications("Summary", baseCounter + " low-priority notifications awaiting notice.");
                
                // Log Success
                Handles.Success("Summary", "Created summary notification for " + baseCounter + " low-priority notifications");
                
                // Set Key
                Utility.Storage('baseNotifications', true);
                
            }
            
        }
        function compileBaseSchema(template) {
            
            // Check for Base Schema
            if (baseSchema) {
                
                // Send to Compile
                compileBaseCompile(template, baseSchema);
                
                // Log Success
                Handles.Success("Base", "Found a schema in storage");
                
            } else {
                
                $.getJSON('api/schemas/base.json', function (data) {
                    
                    // Send to Compile
                    compileBaseCompile(template, data);
                    
                    // Save to Storage.
                    Utility.Storage("baseSchema", JSON.parse(data));
                    
                    // Log Success
                    Handles.Success("Base", "Retrieved and cached a schema");
                    
                }).fail(function () {
                    
                    // Template with Error
                    compileBaseCompile(C.LOADER_TEMPLATE, {"location": "base"});
                    
                    // Log Failure
                    Handles.Failure("Base", "Failed to retrieve a schema");
                    
                });
            }
        }
        
        if (baseTemplate) {
            
            // Send to Compile
            compileBaseSchema(baseTemplate);
            
            // Log Success
            Handles.Success("Base", "Found a template in storage");
            
        } else {
            
            $.get('api/templates/base.txt', function (data) {
               
                // Send to Compile
                compileBaseSchema(data);
                
                // Save to Storage
                Utility.Storage("baseTemplate", data);
                
                // Log Success
                Handles.Success("Base", "Retrieved and cached a template");
                
            }).done(function () {
                
                // Template with Error
                compileBaseCompile(C.ERROR_TEMPLATE, {"location": "base"});
                
                // Log Error
                Handles.Failure("Base", "Failed to retrieve a template");
                
            });
        }
    }
    
    function compilePage(name, target) {
        
        // Case Insensitize
        name = name.toLowerCase();
        
        var pageTemplate = Utility.Storage(name + 'Template'),
            pageSchema = Utility.Storage(name + 'Schema'),

            // Workers or Request
            externalTemplateAgent = null,
            externalSchemaAgent = null;
        
        function compilePageCompiler(template, context) {
            Handles.Template(context, template, target.html(null));
        }
        
        function compilePageSchema(template) {
            
            if (pageSchema) {
                
                // Log Success
                Handles.Success(name.toUpperCase(), "Found a schema in storage");
                
                // Send to Next Step
                compilePageCompiler(template, JSON.parse(pageSchema));
                
            } else {
                    
                $.get('api/schemas/' + name + '.json', function (data) {

                    // Log Success
                    Handles.Success(name.toUpperCase(), "Directly retrieved a schema");

                    // Save to Storage
                    Utility.Storage(name + 'Schema', JSON.stringify(data));

                    // Send to Next Step
                    compilePageCompiler(template, data);

                }).fail(function () {
                    
                    // Tempalate w/ Error
                    compilePageCompiler(C.ERROR_TEMPLATE, {"location" : "page"});

                    // Log Failure
                    Handles.Failure(name.toUpperCase(), "Failed to directly retrieve a schema");
                    
                });
            }
            
        }
        
        if (pageTemplate) {
            
            // Log Success
            Handles.Success(name.toUpperCase(), "Found a template in storage");
            
            // Send to Next Step
            compilePageSchema(pageTemplate);
            
        } else {
            
            // Check Worker Compatibility
            $.get('api/templates/' + name + '.txt', function (data) {
                
                // Log Success
                Handles.Success(name.toUpperCase(), "Retrieved and cached a template");

                // Save to Storage
                Utility.Storage(name + 'Template', data);

                // Send to Next Step
                compilePageSchema(data);
                
            }).fail(function () {
                
                // Template w/ Error
                compilePageCompiler(C.ERROR_TEMPLATE, {"location" : "page"});

                // Log Failure
                Handles.Failure(name.toUpperCase(), "Failed to retrieve a template");
                
            });
            
        }
    }

    function compileArea() {
        var compilation = Handles.Template({}, C.AREA_TEMPLATE, $('#user-area').html(null));
        compilation.find('#log-in').click(Compiler.Base);
    }

    function compileRefresh() {
        compileArea();
        compileBase();

        sessionStorage.removeItem('baseSchema');

        Handles.Success("Base", "Refreshed");

        setTimeout(function () {
            $('#notifications-container').collapse("show");
        }, 400);
    }


    return {
        Base: compileBase,
        Page: compilePage,
        Exit: compileArea,
        Refresh: compileRefresh
    };

}());