/*global $, Notification, moment*/
var Utility = (function () {
    "use strict";

    var Handles = (function () {

        function handleThousands(value) {
            return String(value).replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }

        function handleUppercasing(value) {
            return value.substring(0, 1).toUpperCase() + value.substring(1, value.length).toLowerCase();
        }

        function handleConsole(type, timestamp, log) {

            // Case Insensitize 
            type = type.toLowerCase();

            if (type === "success") {
                window.console.log("%c Moonsquads %c " + handleUppercasing(type) + " @ " + timestamp + " %c " + log,
                    'color: #FFFFFF; background-color: #191919; border-radius: 2px',
                    "background-color: #329932; border-radius: 2px; color: #FFFFFF; margin-left: 5px;",
                    "background-color: white; border-radius: 2px; color: #000000; margin-left: 5px;");
            } else if (type === "failure") {
                window.console.log("%c Moonsquads %c " + handleUppercasing(type) + " @ " + timestamp + " %c " + log,
                    'color: #FFFFFF; background-color: #191919; border-radius: 2px',
                    "background-color: #FF0000; border-radius: 2px; color: #FFFFFF; margin-left: 5px;",
                    "background-color: white; border-radius: 2px; color: #000000; margin-left: 5px;");
            } else if (type === "neutral") {
                window.console.log("%c Moonsquads %c " + handleUppercasing(type) + " @ " + timestamp + " %c " + log,
                    'color: #FFFFFF; background-color: #191919; border-radius: 2px',
                    "background-color: grey; border-radius: 2px; color: #FFFFFF; margin-left: 5px;",
                    "background-color: white; border-radius: 2px; color: #000000; margin-left: 5px;");
            }
        }

        function handleNumerics(numeric) {
            return parseInt(numeric, 10);
        }

        function handlesConversion(date, offset) {
            var timezoneCalculation = date.utcOffset() * 60000,
                offsetCalculation = 3600000 * handleNumerics(offset);

            return moment(date.getTime() + (timezoneCalculation * offsetCalculation));
        }

        function handlesAllocation(day, month, year, minute, second) {
            return moment(day, month, year, minute, second);
        }

        function handlesNavigation() {
            $('#navigation-area').removeAttr('style');
        }

        function handleContainerIn() {
            $("aside").css('transform', 'matrix(1, 0, 0, 1, 0, 0)');
            $("main").css('transform', 'matrix(1, 0, 0, 1, -300, 0)');
        }

        function handleContainerOut() {
            $("aside").removeAttr('style');
            $("main").removeAttr('style');
        }

        function handleCollapses() {
            $('#user-preview').collapse('hide');
            $('#notifications-container').collapse('hide');
            $('#statistics').collapse('hide');
        }
        
        function handleStorage(key, value) {
            if (value) {
                sessionStorage.setItem(key, value);
            }

            return sessionStorage.getItem(key);
        }

        return {
            Thousands: handleThousands,
            Uppercasing: handleUppercasing,
            Console: handleConsole,
            Numerics: handleNumerics,
            Navigation: handlesNavigation,
            Container: {
                In: handleContainerIn,
                Out: handleContainerOut
            },
            Collapses: handleCollapses,
            Storage: handleStorage
        };

    }());

    function handleEnables() {
        
        $('[data-tooltip="tooltip"]').tooltip({
            trigger: "hover"
        });
        
        $('[data-toggle="popover"]').popover();
    
    }
    
    function handleTitles(value) {
        if (value.includes(" ")) {
            var iterator = null,
                strings = value.split(" "),
                result = "";

            for (iterator = 0; iterator < strings.length; iterator += 1) {
                result += Handles.Uppercasing(strings[iterator]) + " ";
            }

            return result.trim();
        } else {
            return Handles.Uppercasing(value);
        }
    }

    function handleSuccess(log) {
        Handles.Console("Success", moment().format('HH:MM:SS'), log);
    }

    function handleFailure(log) {
        Handles.Console("Failure", moment().format('HH:MM:SS'), log);
    }

    function handleNeutral(log) {
        Handles.Console("Neutral", moment().format('HH:MM:SS'), log);
    }

    function handleNotificationsCounter(amount) {
        if (typeof amount === 'number') {
            $('.badge-notifications').html(amount);
        }

        return Handles.Numerics($('.badge-notifications').html());
    }

    function handleNotificationCaret() {
        setTimeout(function () {
            if ($('#notifications-clear-container').attr("aria-expanded") === "true") {
                $('#notifications-clear > a > div > p > span').css('transform', 'rotate(0deg)');
            } else {
                $('#notifications-clear > a > div > p > span').css('transform', 'rotate(180deg)');
            }
        }, 100);
    }

    function handleNotificationsDismissal(notification) {

        // Set Target
        var $target = $('#' + notification);

        $target.animate({

            //Animate
            step: $target.css('transform', 'translate(300px)')

        }, 400, function () {

            // Remove from DOM
            $target.remove();

            // Update Counter
            handleNotificationsCounter($('#notifications').children().length);

            setTimeout(function () {
                if (handleNotificationsCounter() === 0) {
                    Handles.Navigation();
                }
            }, 10);
        });
    }

    function handleNotificationClearance() {

        $('div[id^="not-"]').each(function () {
            handleNotificationsDismissal($(this).attr('id'));
        });

        // Close Container
        setTimeout(function () {
            $('#notifications-clear-container').collapse("hide");
        }, 400);

    }

    function handleContainerToggle() {

        if ($("aside").css('transform') === 'matrix(1, 0, 0, 1, 300, 0)') {
            Handles.Container.In();
        } else {
            Handles.Collapses();
            Handles.Navigation();
            setTimeout(function () {
                Handles.Container.Out();
            }, 200);
        }
    }

    function handleContainerFocus() {
        Handles.Collapses();
        Handles.Navigation();
        setTimeout(function () {
            Handles.Container.Out();
        }, 200);
    }

    function handleNavigation() {

        function handleNavigationNotifications() {
            if (handleNotificationsCounter() === 0) {
                $('#navigation-area').css('overflow', 'hidden');
            }
        }

        setTimeout(function () {
            if ($('#notifications').attr('aria-expanded') === "true") {
                handleNavigationNotifications();
            } else if ($('#statistics').attr('aria-expanded') === "true") {
                $('#navigation-area').css('overflow', 'hidden');
            } else {
                Handles.Navigation();
            }
        }, 20);
    }
    
    function handlePopovers(e) {
        $('[data-toggle="popover"],[data-original-title]').each(function () {
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                (($(this).popover('hide').data('bs.popover') || {}).inState || {}).click = false;
            }

        });
    }

    return {
        Thousands: Handles.Thousands,
        Titles: handleTitles,
        Success: handleSuccess,
        Failure: handleFailure,
        Neutral: handleNeutral,
        Notifications: {
            Caret: handleNotificationCaret,
            Dismiss: handleNotificationsDismissal,
            Clear: handleNotificationClearance
        },
        Container: {
            Toggle: handleContainerToggle,
            Focus: handleContainerFocus
        },
        Navigation: handleNavigation,
        Collapses: Handles.Collapses,
        Storage: Handles.Storage,
        Enables: handleEnables,
        Popovers: handlePopovers
    };


}());