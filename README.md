# What is Landing?
Produced by the  developers of the Moonsquads project, Landing is the first release of a site powered by the Handlebars front-end templating engine, Bootstrap framework, and a Node.js REST API in conjunction  with Metalsmith for static site generation.

Landing was purposed with giving the target audience of Moonsquads an outlook on what is currently going on in the community, and provide the staff team with ann application that would allow them to reach out to the target audience in a formal manner - through a main page with info on current goings-on, being able to create pages about their favorite games, and release press on whatever they felt was important enough to release.
