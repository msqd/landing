/*global $, Chart, CountUp*/

$(document).ready(function () {
    "use strict";
    

    var uptimeChart = new Chart($('#uptime canvas'), {
            type: 'bar',
            data: {
                labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"],
                datasets: [
                    {
                        label: 'Hours Up',
                        data: [
                            24,
                            24,
                            4,
                            24,
                            24
                        ],
                        backgroundColor: [
                            'rgb(76,166,76)',
                            'rgb(76,166,76)',
                            'rgb(76,166,76)',
                            'rgb(76,166,76)',
                            'rgb(76,166,76)'
                        ],
                        borderColor: [
                            'rgb(76,166,76)',
                            'rgb(76,166,76)',
                            'rgb(76,166,76)',
                            'rgb(76,166,76)',
                            'rgb(76,166,76)'
                        ],
                        borderWidth: 1
                    },
                    {
                        label: 'Hours Down',
                        data: [
                            24,
                            24,
                            16,
                            24,
                            24
                        ],
                        backgroundColor: [
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)'
                        ],
                        borderColor: [
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)',
                            'rgb(255,76,76)'
                        ],
                        borderWidth: 1
                    }
                    
                ]
            },
            options: {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true
                            }
                        }
                    ]
                }
            }
        }),
        implementChart = new Chart($('#implementations canvas'), {
            type: 'pie',
            data: {
                labels: ["Users", "Administration", "Tools"],
                datasets: [
                    {
                        data: [300, 50, 100],
                        backgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ],
                        hoverBackgroundColor: [
                            "#FF6384",
                            "#36A2EB",
                            "#FFCE56"
                        ]
                    }
                ]
            },
            options: {
                scales: {
                    yAxes: [
                        {
                            ticks: {
                                beginAtZero: true
                            }
                        }
                    ]
                }
            }
        }),
        counter = new CountUp("counter", 0, 23900);
    
    counter.start();

    
});