/*global $, Worker, Bloodhound, Handlebars, Utility, Countdown*/
$(document).ready(function () {
    "use strict";

    $('[data-toggle="tab"]').first().tab('show');
    $('[data-toggle="popover"]').popover();

    $(document).on('typeahead:autocomplete', function () {
        $('#input-search').typeahead('close');
    });

    $('.btn-banner').click(function () {

        if (!$('#input-search').val()) {
            $('.alert-banner').fadeIn(200, function () {
                setTimeout(function () {
                    $(this).hide();
                }, 1000);
            });
        } else {
            var enteredSearch = $('#input-search').val().toLowerCase(),
                actualSearch = enteredSearch.substring(enteredSearch.indexOf(" ") + 1, enteredSearch.length).split(' '),
                alteredSearch = "";

            actualSearch.forEach(function (word) {
                alteredSearch += word + "+";

                if (actualSearch.indexOf(word) === actualSearch.length - 1) {
                    alteredSearch = alteredSearch.substring(0, alteredSearch.length - 1);
                }
            });

            window.location.search = "search=" + alteredSearch;
        }
    });
    $('.btn-banner:last-child').click(function () {
        Utility.Storage('advancedSearch', true);
    });
    $('.alert-banner').click(function () {
        $(this).fadeOut(200, function () {
            $(this).hide();
        });
    });

    Handlebars.registerHelper('mainUsername', function (username) {
        if (username.length > 12) {
            return username.substring(0, 9) + "... ";
        } else {
            return username;
        }
    });
    Handlebars.registerHelper('mainPlaying', function (username) {
        if (username.length > 15) {
            return username.substring(0, 15);
        } else {
            return username;
        }
    });

    $.getJSON('api/search.json', function (results) {

        var searchResults = [],
            searchAgent = null,
            searchOptions = {
                highlight: true
            };

        function formatSuggestion(result) {
            var x = {
                type: result.substring(result.indexOf("["), result.indexOf("]") + 1),
                text: result.substring(result.indexOf(" ") + 1, result.length)
            };

            return '<div><strong>' + x.type + '</strong> ' + x.text + '</div>';
        }

        results.forEach(function (result) {
            searchResults.push("[" + result.type + "] " + result.text);
        });

        searchAgent = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            local: searchResults
        });

        searchAgent.initialize();

        $('#input-search').typeahead(searchOptions, {
            source: searchAgent.ttAdapter(),
            templates: {
                suggestion: formatSuggestion,
                empty: '<div class="tt-emptysug">That doesnt match anything!</div>'
            },
            limit: 3
        });

    });
    $.getJSON('api/discord.php', function (discordData) {
        $.get('cdn/templates/discord.txt', function (discordTemplate) {
            $(Handlebars.compile(discordTemplate)(discordData)).appendTo($('#dashboard #discord > div'));
        });
    });


    $('p[class*=count]').each(function () {
        var a = new Countdown({
            selector: '.' + $(this).attr("class"),
            dateEnd: new Date($(this).text()),
            msgPattern: 'Ending in {days}d, {hours}h, and {minutes}m'
        });
    });


});